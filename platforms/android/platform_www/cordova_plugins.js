cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-device-motion.Acceleration",
      "file": "plugins/cordova-plugin-device-motion/www/Acceleration.js",
      "pluginId": "cordova-plugin-device-motion",
      "clobbers": [
        "Acceleration"
      ]
    },
    {
      "id": "cordova-plugin-device-motion.accelerometer",
      "file": "plugins/cordova-plugin-device-motion/www/accelerometer.js",
      "pluginId": "cordova-plugin-device-motion",
      "clobbers": [
        "navigator.accelerometer"
      ]
    },
    {
      "id": "cordova-plugin-device-orientation.CompassError",
      "file": "plugins/cordova-plugin-device-orientation/www/CompassError.js",
      "pluginId": "cordova-plugin-device-orientation",
      "clobbers": [
        "CompassError"
      ]
    },
    {
      "id": "cordova-plugin-device-orientation.CompassHeading",
      "file": "plugins/cordova-plugin-device-orientation/www/CompassHeading.js",
      "pluginId": "cordova-plugin-device-orientation",
      "clobbers": [
        "CompassHeading"
      ]
    },
    {
      "id": "cordova-plugin-device-orientation.compass",
      "file": "plugins/cordova-plugin-device-orientation/www/compass.js",
      "pluginId": "cordova-plugin-device-orientation",
      "clobbers": [
        "navigator.compass"
      ]
    },
    {
      "id": "cordova-plugin-gyroscope.Orientation",
      "file": "plugins/cordova-plugin-gyroscope/www/Orientation.js",
      "pluginId": "cordova-plugin-gyroscope",
      "clobbers": [
        "Orientation"
      ]
    },
    {
      "id": "cordova-plugin-gyroscope.gyroscope",
      "file": "plugins/cordova-plugin-gyroscope/www/gyroscope.js",
      "pluginId": "cordova-plugin-gyroscope",
      "clobbers": [
        "navigator.gyroscope"
      ]
    },
    {
      "id": "cordova-plugin-magnetometer.main",
      "file": "plugins/cordova-plugin-magnetometer/www/magnetometer.js",
      "pluginId": "cordova-plugin-magnetometer",
      "clobbers": [
        "cordova.plugins.magnetometer"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.Coordinates",
      "file": "plugins/cordova-plugin-indooratlas/www/Coordinates.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.Coordinates"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.Orientation",
      "file": "plugins/cordova-plugin-indooratlas/www/Orientation.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.Orientation"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.CurrentStatus",
      "file": "plugins/cordova-plugin-indooratlas/www/CurrentStatus.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.CurrentStatus"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.Position",
      "file": "plugins/cordova-plugin-indooratlas/www/Position.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.Position"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.FloorPlan",
      "file": "plugins/cordova-plugin-indooratlas/www/FloorPlan.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.FloorPlan"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.Venue",
      "file": "plugins/cordova-plugin-indooratlas/www/Venue.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.Venue"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.CoordinateTransforms",
      "file": "plugins/cordova-plugin-indooratlas/www/CoordinateTransforms.js",
      "pluginId": "cordova-plugin-indooratlas"
    },
    {
      "id": "cordova-plugin-indooratlas.RegionChangeObserver",
      "file": "plugins/cordova-plugin-indooratlas/www/RegionChangeObserver.js",
      "pluginId": "cordova-plugin-indooratlas"
    },
    {
      "id": "cordova-plugin-indooratlas.RoutingLeg",
      "file": "plugins/cordova-plugin-indooratlas/www/RoutingLeg.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.RoutingLeg"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.RoutingPoint",
      "file": "plugins/cordova-plugin-indooratlas/www/RoutingPoint.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.RoutingPoint"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.Route",
      "file": "plugins/cordova-plugin-indooratlas/www/Route.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.Route"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.Geofence",
      "file": "plugins/cordova-plugin-indooratlas/www/Geofence.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.Geofence"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.POI",
      "file": "plugins/cordova-plugin-indooratlas/www/POI.js",
      "pluginId": "cordova-plugin-indooratlas",
      "clobbers": [
        "IndoorAtlas.POI"
      ]
    },
    {
      "id": "cordova-plugin-indooratlas.IndoorAtlas",
      "file": "plugins/cordova-plugin-indooratlas/www/IndoorAtlas.js",
      "pluginId": "cordova-plugin-indooratlas",
      "merges": [
        "IndoorAtlas"
      ]
    },
    {
      "id": "cordova-plugin-sensors.sensors",
      "file": "plugins/cordova-plugin-sensors/www/sensors.js",
      "pluginId": "cordova-plugin-sensors",
      "clobbers": [
        "sensors"
      ]
    },
    {
      "id": "cordova-plugin-file.DirectoryEntry",
      "file": "plugins/cordova-plugin-file/www/DirectoryEntry.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.DirectoryEntry"
      ]
    },
    {
      "id": "cordova-plugin-file.DirectoryReader",
      "file": "plugins/cordova-plugin-file/www/DirectoryReader.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.DirectoryReader"
      ]
    },
    {
      "id": "cordova-plugin-file.Entry",
      "file": "plugins/cordova-plugin-file/www/Entry.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.Entry"
      ]
    },
    {
      "id": "cordova-plugin-file.File",
      "file": "plugins/cordova-plugin-file/www/File.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.File"
      ]
    },
    {
      "id": "cordova-plugin-file.FileEntry",
      "file": "plugins/cordova-plugin-file/www/FileEntry.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileEntry"
      ]
    },
    {
      "id": "cordova-plugin-file.FileError",
      "file": "plugins/cordova-plugin-file/www/FileError.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileError"
      ]
    },
    {
      "id": "cordova-plugin-file.FileReader",
      "file": "plugins/cordova-plugin-file/www/FileReader.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileReader"
      ]
    },
    {
      "id": "cordova-plugin-file.FileSystem",
      "file": "plugins/cordova-plugin-file/www/FileSystem.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileSystem"
      ]
    },
    {
      "id": "cordova-plugin-file.FileUploadOptions",
      "file": "plugins/cordova-plugin-file/www/FileUploadOptions.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileUploadOptions"
      ]
    },
    {
      "id": "cordova-plugin-file.FileUploadResult",
      "file": "plugins/cordova-plugin-file/www/FileUploadResult.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileUploadResult"
      ]
    },
    {
      "id": "cordova-plugin-file.FileWriter",
      "file": "plugins/cordova-plugin-file/www/FileWriter.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.FileWriter"
      ]
    },
    {
      "id": "cordova-plugin-file.Flags",
      "file": "plugins/cordova-plugin-file/www/Flags.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.Flags"
      ]
    },
    {
      "id": "cordova-plugin-file.LocalFileSystem",
      "file": "plugins/cordova-plugin-file/www/LocalFileSystem.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.LocalFileSystem"
      ],
      "merges": [
        "window"
      ]
    },
    {
      "id": "cordova-plugin-file.Metadata",
      "file": "plugins/cordova-plugin-file/www/Metadata.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.Metadata"
      ]
    },
    {
      "id": "cordova-plugin-file.ProgressEvent",
      "file": "plugins/cordova-plugin-file/www/ProgressEvent.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.ProgressEvent"
      ]
    },
    {
      "id": "cordova-plugin-file.fileSystems",
      "file": "plugins/cordova-plugin-file/www/fileSystems.js",
      "pluginId": "cordova-plugin-file"
    },
    {
      "id": "cordova-plugin-file.requestFileSystem",
      "file": "plugins/cordova-plugin-file/www/requestFileSystem.js",
      "pluginId": "cordova-plugin-file",
      "clobbers": [
        "window.requestFileSystem"
      ]
    },
    {
      "id": "cordova-plugin-file.resolveLocalFileSystemURI",
      "file": "plugins/cordova-plugin-file/www/resolveLocalFileSystemURI.js",
      "pluginId": "cordova-plugin-file",
      "merges": [
        "window"
      ]
    },
    {
      "id": "cordova-plugin-file.isChrome",
      "file": "plugins/cordova-plugin-file/www/browser/isChrome.js",
      "pluginId": "cordova-plugin-file",
      "runs": true
    },
    {
      "id": "cordova-plugin-file.androidFileSystem",
      "file": "plugins/cordova-plugin-file/www/android/FileSystem.js",
      "pluginId": "cordova-plugin-file",
      "merges": [
        "FileSystem"
      ]
    },
    {
      "id": "cordova-plugin-file.fileSystems-roots",
      "file": "plugins/cordova-plugin-file/www/fileSystems-roots.js",
      "pluginId": "cordova-plugin-file",
      "runs": true
    },
    {
      "id": "cordova-plugin-file.fileSystemPaths",
      "file": "plugins/cordova-plugin-file/www/fileSystemPaths.js",
      "pluginId": "cordova-plugin-file",
      "merges": [
        "cordova"
      ],
      "runs": true
    },
    {
      "id": "cordovarduino.Serial",
      "file": "plugins/cordovarduino/www/serial.js",
      "pluginId": "cordovarduino",
      "clobbers": [
        "window.serial"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-plugin-device-motion": "2.0.1",
    "cordova-plugin-device-orientation": "2.0.1",
    "cordova-plugin-gyroscope": "0.1.4",
    "cordova-plugin-magnetometer": "1.0.0",
    "cordova-plugin-indooratlas": "3.3.0",
    "cordova-plugin-sensors": "0.7.0",
    "cordova-plugin-file": "6.0.2",
    "cordovarduino": "0.0.10"
  };
});