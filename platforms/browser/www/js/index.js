/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

 /*
 * Author: Julien Caverne
 * Date:   24/09/2020
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

var timer = null;
var position;
var acceleration = navigator.accelerometer;
var gyroscope = navigator.gyroscope;
var magnetometer = navigator.magnetometer;
var sensors = navigator.orientation;
var rotation = navigator.rotationvector;
var constraints;

var saveFile;
var errorsFile;

///var IndoorAtlas = navigator.indooratlas;

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);

    // Removing file from last session
    clearFile('sessionData.csv');
    setTimeout(clearFile, 100, 'errors.csv');

    // Creating a file to save data
    setTimeout(createDataFile, 200);
    setTimeout(createErrorsFile, 300);
    
    setTimeout(setArduinoConnexion, 400);

    setTimeout(beginMeasures, 500);
}

// MAIN FUNCTION

function handleButtonEvent() {
    //Set position in html
    if (position) {
        document.getElementById('gpsData').innerHTML = ""
        + "   timestamp: " + position.timestamp
        + "\n   lati: "      + position.coords.latitude
        + "\n   long: "      + position.coords.longitude
        + "\n   alti: "      + position.coords.altitude
        + "\n   heading: "   + position.coords.heading
        + "\n   speed: "     + position.coords.speed
        + "\n   accuracy: "  + position.coords.accuracy
    }
    //Set acceleration in html
    document.getElementById('accelerometerData').innerHTML = ""
    + "   timestamp: " + acceleration.timestamp
    + "\n   X: "      + acceleration.x
    + "\n   Y: "      + acceleration.y
    + "\n   Z: "      + acceleration.z;

    //Set gyroscope in html
    // document.getElementById('gyroscopeData').innerHTML = ""
    // + "   timestamp: " + gyroscope.timestamp
    // + "\n   X: "      + gyroscope.x
    // + "\n   Y: "      + gyroscope.y
    // + "\n   Z: "      + gyroscope.z;

    //Set magnetometer in html
    // document.getElementById('magnetometerData').innerHTML = ""
    // + "   timestamp: " + acceleration.timestamp
    // + "\n   X: "      + magnetometer.x
    // + "\n   Y: "      + magnetometer.y
    // + "\n   Z: "      + magnetometer.z;

    serial.read(function(buffer) {console.log(buffer);}, error => handleError(`Error while reading data from Arduino`, error));

    save(position, acceleration, gyroscope, magnetometer);
}

function startOrStopTimer() {
    if (!timer) {
        timer = setInterval(handleButtonEvent, 1000) 
    } else {
        clearInterval(timer);
        timer = null;
    }
}

// SETUP FUNCTIONS

function beginMeasures() {
    //Add volume buttons events:
    document.addEventListener("volumeupbutton", handleButtonEvent, false);
    document.addEventListener("volumedownbutton", handleButtonEvent, false);
    //start watching gps
    navigator.geolocation.watchPosition((pos) => {position = pos;}, (error) => handleError(`Error while setting GPS`, error), {timeout: 1000, enableHighAccuracy: true });
    //start watching acceleration
    navigator.accelerometer.watchAcceleration((acc) => {acceleration = acc;}, (error) => handleError(`Error while setting accelerometer`, error), { frequency: 100 });
    //start watching gyroscope
    navigator.gyroscope.watch((gyro) => {gyroscope = gyro;}, (error) => handleError(`Error while setting gyroscope`, error), { frequency: 100 });
    //start watching magneto
    cordova.plugins.magnetometer.watchReadings((magn) => {magnetometer = magn;}, (error) => handleError(`Error while setting magnetometer`, error), { frequency: 100 });
}

function setArduinoConnexion() {
    serial.requestPermission(function(successMessage) {
        serial.open({}, function(successMessage) {
            console.log(successMessage);
        }, error => handleError(`Error while opening serial link`, error));
    }, error => handleError(`Error while getting permission for serial link`, error));
}

// DATA MANAGING FUNCTIONS

function save(positionData, accelerationData, gyroscopeData, magnetometerData) {
    newLine = `${positionData.timestamp};
        ${positionData.coords.latitude};
        ${positionData.coords.longitude};
        ${positionData.coords.altitude};
        ${positionData.coords.heading};
        ${positionData.coords.speed};
        ${positionData.coords.accuracy};
        ${accelerationData.timestamp};
        ${accelerationData.x};
        ${accelerationData.y};
        ${accelerationData.z}\n`;
        // ${gyroscopeData.timestamp};
        // ${gyroscopeData.x};
        // ${gyroscopeData.y};
        // ${gyroscopeData.z};
        // ${accelerationData.timestamp};
        // ${magnetometerData.x};
        // ${magnetometer.y};
        // ${magnetometer.z}\n`;

    writeFile(saveFile, new Blob([newLine]));
    readFile(saveFile);
    readFile(errorsFile);
}

// FILE MANAGING FUNCTIONS

function clearFile(name) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
        fs.root.getFile(name, {create: false, exclusive: false}, function(fileEntry) {
            fileEntry.remove(function(file) {console.log('File removed')}, error => handleError(`Error while deleting file ${name}`, error));
        }, error => handleError(`Error while getting file ${name}`, error));
    }, error => handleError(`Error while using file manager`, error));
}

function createDataFile() {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
        fs.root.getFile('sessionData.csv', {create: true, exclusive: true}, function(fileEntry) {
            saveFile = fileEntry;
            writeFile(saveFile, new Blob([
                `position timestamp;latitude;longitude;altitude;heading;speed;accuracy;
                acceleration timestamp;acceleration x;acceleration y;acceleration z\n`
                // gyrorsope timestamp;gyroscope x;gyroscope y;gyroscope z;
                // magnetometer timestamp;magnetometer x;magnetometer y;magnetometer z;
                // constraint 1;constraint 2;constraint 3;constraint 4;constraint 5;constraint 6\n`
            ]));
            console.log('Data file opened');
        }, error => handleError(`Error while getting file ${fileEntry.name}`, error));
    }, error => handleError(`Error while using file manager`, error));
}

function createErrorsFile() {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
        fs.root.getFile('errors.csv', {create: true, exclusive: true}, function(fileEntry) {
            errorsFile = fileEntry;
            writeFile(errorsFile, new Blob(['time;message;error\n']));
            console.log('Errors file opened');
        }, error => handleError(`Error while getting file ${fileEntry.name}`, error));
    }, error => handleError(`Error while using file manager`, error));
}

function writeFile(fileEntry, dataObj) {
    fileEntry.createWriter(function(fileWriter) {
        fileWriter.onwritend = function() {
            console.log('Writing successful');
        }
        fileWriter.onerror = function() {
            handleError(`Error while writing in file ${fileEntry.name}`, error);
        }

        if (!dataObj) {
            handleError(`Error while writing in file ${fileEntry.name} : no data to write`, error);
        }

        fileWriter.seek(fileWriter.length);
        fileWriter.write(dataObj);
    });
}

function readFile(fileEntry) {
    fileEntry.file(function(file) {
        var reader = new FileReader();

        reader.onloadend = function() {
            console.log('Success : \n' + this.result);
        }

        reader.readAsText(file);
    }, error => handleError(`Error while reading ${fileEntry.name}`, error));
}

// ERROR MANAGING

function handleError(message, error) {
    var errorMessage = typeof(error) === 'string' ? error : error.message;
    writeFile(errorsFile, new Blob([`${new Date()};${message};${errorMessage}\n`]));

    document.getElementById("errorTime").innerHTML = new Date();
    document.getElementById("errorMessage").innerHTML = `${message} ; ${errorMessage}`;
}
